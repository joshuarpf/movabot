// Package validator : implements the input validation
package validator

import (
	"fmt"
	"movabot/command"
	"movabot/config"
	"movabot/helper"
	"regexp"
	"strconv"
	"strings"
)

/*
areCoordinatesWithinBounds : Checks againts config and returns whether the given coordinates are within bounds

@params		xCoordinate		represents value of x
@params		yCoordinate		represents value of y

@return									boolean
*/
func areCoordinatesWithinBounds(xCoordinate int, yCoordinate int) bool {
	// Verify X-Coodinate
	if (xCoordinate >= config.MinX) && (xCoordinate <= config.MaxX) {
		// verify Y-Coordinate
		if (yCoordinate >= config.MinY) && (yCoordinate <= config.MaxY) {
			return true
		}
	}

	return false
}

/*
getKeywords : Returns an array of acceptible keywords
*/
func getKeywords() []command.Keyword {
	var keywords = []command.Keyword{
		command.LEFT,
		command.MOVE,
		command.PLACE,
		command.QUIT,
		command.REPORT,
		command.RIGHT,
	}

	return keywords
}

/*
isValidPlaceCommandFormat : verifies the validity of a plot command with a given expression

@params		input		inputted plot command
@output						boolean
*/
func isValidPlaceCommandFormat(input string) bool {
	match, _ := regexp.MatchString(`PLACE ((\s)+)?[0-9]+((\s)+)?,((\s)+)?[0-9]+((\s)+)?,((\s)+)?(EAST|NORTH|SOUTH|WEST)`, input)
	return match
}

/*
IsValidInput : this method will check the validity of an input

@params		input		inputted string
@output						boolean
*/
func IsValidInput(input string) bool {

	keywords := getKeywords()

	for _, validKeyword := range keywords {
		if strings.HasPrefix(input, validKeyword.String()) {
			if validKeyword.String() == "PLACE" {
				if veirfyPlaceCommand(input) == true {
					return true
				}
				return false
			}
			return true
		}
	}
	return false
}

/*
veirfyPlaceCommand : Verifies if the input is a valid plot command

@params		input		inputted plot command
@output						boolean
*/
func veirfyPlaceCommand(input string) bool {
	if strings.HasPrefix(input, "PLACE") {
		if isValidPlaceCommandFormat(input) {
			container := helper.ExtractCoordinatesFromPlaceString(input)

			xCoordinate, error := strconv.Atoi(container[0])
			if error != nil {
				fmt.Println("The given x-coordinate is not a valid input")
				return false
			}

			yCoordinate, error := strconv.Atoi(container[1])
			if error != nil {
				fmt.Println("the given y-coordinate is not a valid input")
				return false
			}

			if areCoordinatesWithinBounds(xCoordinate, yCoordinate) == true {
				return true
			}
		}
	}

	return false
}
