package main

import (
	"fmt"
	"movabot/bot"
	"movabot/direction"
	"movabot/helper"
	"movabot/validator"
	"reflect"
	"strconv"
	"strings"
	"testing"
)

/*
assertEqual : Checks if the values are equal
*/
func assertEqual(tester *testing.T, a interface{}, b interface{}, message string) {
	if a == b {
		return
	}

	if len(message) == 0 {
		message = fmt.Sprintf("%v != %v", a, b)
	}
	tester.Fatal(message)
}

func isNumeric(s string) bool {
	_, error := strconv.ParseFloat(s, 64)
	return error == nil
}

/*
TestCleanupInput : run tests on the cleaning up of input

@params		tester		testing object
*/
func TestCleanupInput(tester *testing.T) {
	tester.Run("Test capitalizing of all letters", func(tester *testing.T) {
		got := cleanupInput("left")
		want := "LEFT"

		assertEqual(tester, got, want, "Did not produce the expected capitalized command")
	})

	tester.Run("Test capitalizing of strings with spaces", func(tester *testing.T) {
		got := cleanupInput(`report`)
		want := "REPORT"

		assertEqual(tester, got, want, "Did not produce the expected capitalized command")
	})

	tester.Run("Test if the inputted strings get trimmed", func(tester *testing.T) {
		got := cleanupInput("     move    ")
		want := "MOVE"

		assertEqual(tester, got, want, "Did not produce the trimmed string")
	})
}

/*
EvaluateInput : Tests fo the method that evaluates user input
*/
func EvaluateInput(tester *testing.T) {
	tester.Run("Test to verify that nothing happens if it is given an invalid command", func(tester *testing.T) {
		robot := &bot.Bot{}
		robot.EvaluateInput("hellow world")

		// TODO: implement checking of the command line result, when you already know how
	})
}

/*
TestExtractCoordinatesFromPlaceString : tests the helper method to see if it returns the correct breakdown
*/
func TestExtractCoordinatesFromPlaceString(tester *testing.T) {
	tester.Run("Test if it returns an array with the correct number of elements", func(tester *testing.T) {
		result := helper.ExtractCoordinatesFromPlaceString("PLACE 1, 2, NORTH")

		if len(result) != 3 {
			tester.Errorf("It did not return the correct number of parameters")
		}
	})

	tester.Run("Test to see if the resulting string matches the expected results", func(tester *testing.T) {

		directions := []string{"NORTH", "EAST", "SOUTH", "WEST"}

		for _, direction := range directions {

			inputString := "PLACE 0,0," + direction

			result := helper.ExtractCoordinatesFromPlaceString(inputString)

			if isNumeric(result[0]) == false {
				tester.Error("The extracted x-coordinate is not an acceptible value")
			}

			if (isNumeric(result[1])) == false {
				tester.Error("The extracted y-coordinate is not an acceptible value")
			}

			stringToTest := strings.Trim(result[2], " ") // Trims the sides

			if !reflect.DeepEqual(stringToTest, direction) {
				tester.Error("The extracted directional string is not an acceptible value")
			}
		}

	})
}

/*
TestGetIndexFromDirectionsArray : Tests the method that returns the index from a given direction ( a string )
*/
func TestGetIndexFromDirectionsArray(tester *testing.T) {
	tester.Run("Test if the return is a valid integer", func(tester *testing.T) {
		var cardinalDirections = []direction.CardinalDirections{
			direction.NORTH,
			direction.EAST,
			direction.SOUTH,
			direction.WEST,
		}

		for _, direction := range cardinalDirections {
			index := helper.GetIndexFromDirectionsArray(cardinalDirections, direction)

			if index < 0 {
				tester.Errorf("The index returned is not valid")
			}
		}
	})

	tester.Run("Test to see if it still returns an acceptible format if the input array is wrong", func(tester *testing.T) {
		var cardinalDirections = []direction.CardinalDirections{}

		for _, direction := range cardinalDirections {
			index := helper.GetIndexFromDirectionsArray(cardinalDirections, direction)

			if index >= 0 {
				tester.Errorf("It returned the wrong, it may be considered a valid one")
			}
		}
	})
}

/*
TestIsValidInput : run tests on the method that checks validation input validity

@params		tester		testing object
*/
func TestIsValidInput(tester *testing.T) {
	tester.Run("Test if IsValidInput returns boolean", func(tester *testing.T) {

		sampleInputs := []string{"LEFT", "MOVE", "PLACE", "QUIT", "REPORT", "RIGHT", "something", "else"}

		for _, input := range sampleInputs {
			validationOutput := validator.IsValidInput(input)

			if (validationOutput != true) && (validationOutput != false) {
				tester.Errorf("The response was not boolean")
			}
		}
	})

	tester.Run("Test if isValidInput returns true for valid inputs (Except PLACE)", func(tester *testing.T) {

		validInputs := []string{"LEFT", "MOVE", "QUIT", "REPORT", "RIGHT"}

		for _, input := range validInputs {
			isValid := validator.IsValidInput(input)

			if isValid != true {
				tester.Errorf("Validation failed to return true on input %s : %t", input, isValid)
			}
		}

	})

	tester.Run("Test if isValidInput returns false for invalid inputs", func(tester *testing.T) {
		isValid := validator.IsValidInput("something")

		if isValid == true {
			tester.Errorf("Validation failed to return false on an invalid input: %t", isValid)
		}
	})

	tester.Run("Test if isValidInput returns true for valid PLACE format", func(tester *testing.T) {
		isValid := validator.IsValidInput("PLACE 4,5,NORTH")

		if isValid == false {
			tester.Errorf("Validation failed to return true on an invalid PLACE input: %t", isValid)
		}
	})

	tester.Run("Test if isValidInput returns false for invalid PLACE format", func(tester *testing.T) {
		isValid := validator.IsValidInput("PLACE 4,5,5,2,NORTH")

		if isValid == true {
			tester.Errorf("Validation failed to return false on an invalid PLACE input: %t", isValid)
		}
	})
}

/*
TestTrimStringsFromPrefix : Test to see if the suffixes from valid commands are properly stripped out
*/
func TestTrimStringsFromPrefix(tester *testing.T) {
	tester.Run("Test to see if it works for single word commands", func(tester *testing.T) {
		got := helper.TrimStringsFromPrefix("REPORT foo bar")
		want := "REPORT"

		assertEqual(tester, got, want, "It produced an unexpected output")
	})
}
