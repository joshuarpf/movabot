# Movabot

This is a command-line application that simulates a toy robot moving in a tabletop. There are no obstructions on the tabletop, and the bot is free to roam around within its dimensions. The bot follows commands one at a time (for now).

This application was written in Go programming language ( version 1.14 on Linux ). There are no 3rd party dependencies for this project, every package used are built in with go. This will only run fromm a command line. 

Please note that this application was built under a unix environment. The binary packaged with it, might not run on a windows environment. 

# Requirements
1. Installed Go on your system (*for unix environment, you may opt to simply run the binary*)
2. CLI ( Command Line interface )
3. Git

# How to run
1. Clone the repository to the **/src** folder of your GOPATH (*This may differ depending on your OS*)
2. Enter the commands *go run main.go* , this will run the application. (*You should see a welcome message*)
3. You can now start playing witht he bot, however it will ask you set a placement of the bot before anything else. Here are the acceptable comamnds
   - PLACE (number/integer), (number/integer), (NORTH|EAST|SOUTH|WEST) *places the bot on the board* (*Note: commands are automatically converted to upper case upon input*)
   - LEFT *turns the direction of the bot to the left*
   - RIGHT *turns the direction of the bot to the right*
   - MOVE *moves the bot once block to the direction it is facing*
   - REPORT *shows the current position and direction to where the bot is facing*
   - QUIT *exits the application*

**For Unix / Linux Environments**
Note: for unix systems, you can simply run the binary by typing **./main**
