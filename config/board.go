// Package config : contains all static config variables
package config

// MaxX : Max of X-Coordinate
const MaxX = 5

// MinX : Min of X-Coordinate
const MinX = 0

// MaxY : Max of Y-Coordinate
const MaxY = 5

// MinY : Min of Y-Coordinate
const MinY = 0
