// Package bot : provides the features of the simulated bot
package bot

import (
	"fmt"
	"log"
	"movabot/command"
	"movabot/config"
	"movabot/direction"
	"os"
	"strings"
)

/*
EvaluateInput : Evaluates the input for the bot

@params			input 		string input
*/
func (bot *Bot) EvaluateInput(input string) {

	if len(input) <= 0 {
		return
	}

	switch input {
	case command.LEFT.String():
		if bot.isPlaced() {
			bot.turn(input)
		} else {
			bot.showNoPlacementError()
		}

	case command.MOVE.String():
		if bot.isPlaced() {
			bot.move()
		} else {
			bot.showNoPlacementError()
		}

	case command.QUIT.String():
		fmt.Println("Goodbye ...")
		os.Exit(0)

	case command.REPORT.String():
		if bot.isPlaced() {
			bot.showReport()
		} else {
			bot.showNoPlacementError()
		}
	case command.RIGHT.String():
		if bot.isPlaced() {
			bot.turn(input)
		} else {
			bot.showNoPlacementError()
		}

	default:

		if strings.HasPrefix(input, "PLACE") {
			bot.setPlacement(input)
		}

	}
}

/*
move	: executes the movement of the bot
*/
func (bot *Bot) move() {

	currentX := bot.getCurrentXCoordinate()
	currentY := bot.getCurrentYCoordinate()

	if currentX >= 0 && currentY >= 0 {
		switch bot.getCurrentFaceDirection() {
		case direction.NORTH:
			if currentY < config.MaxY {
				currentY++
				bot.setYCoordinate(currentY)
			}
		case direction.EAST:
			if currentX < config.MaxX {
				currentX++
				bot.setXCoordinate(currentX)
			}
		case direction.SOUTH:
			if currentY > config.MinY {
				currentY--
				bot.setYCoordinate(currentY)
			}
		case direction.WEST:
			if currentX > config.MinX {
				currentX--
				bot.setXCoordinate(currentX)
			}
		}
	}
}

/*
showNoPlacementError : Displays a message that the bot has not been placed
*/
func (bot *Bot) showNoPlacementError() {
	fmt.Println("Woops! You must place a bot in order to play")
}

/*
showReport : Shows the current placement of the bot as output
*/
func (bot *Bot) showReport() {
	xCorrdinate, yCoordinate, direction := bot.GetCurrentStatus()

	if xCorrdinate < 0 || yCoordinate < 0 || len(direction.String()) <= 0 {
		log.Fatal("There was a problem fetching values for current status on showReport")
	}

	fmt.Printf("Output: %d, %d, %s\n", xCorrdinate, yCoordinate, direction.String())
}

/*
turn : executes the proucess of turning the direction of the bot

@param 			intput 		directions to where the bot should be facing next
*/
func (bot *Bot) turn(input string) {
	if len(input) <= 0 {
		return
	}

	currentDirection := bot.getCurrentFaceDirection()
	var direction command.Keyword

	if input == command.LEFT.String() {
		direction = command.LEFT
	}

	if input == command.RIGHT.String() {
		direction = command.RIGHT
	}

	nextDirection := bot.getNextDirection(currentDirection, direction)
	bot.setFaceDirection(nextDirection)
}
