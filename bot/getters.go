// Package bot : provides the features of the simulated bot
package bot

import (
	"log"
	"movabot/direction"
	"movabot/helper"
	"strconv"
)

/*
getCoordinatesFromPlaceCommand : method that returns the coordinates derived from the place command

@params		input		string  command for placement

@return		int 		x coordinate
@return 	int 		y coordinate
@return		string	string representation of the direction
*/
func (bot *Bot) getCoordinatesFromPlaceCommand(input string) (int, int, string) {

	if len(input) <= 0 {
		return -1, -1, ""
	}

	container := helper.ExtractCoordinatesFromPlaceString(input)

	// Initiate coordinates ot return
	x, error := strconv.Atoi(container[0])
	if error != nil {
		log.Fatal("Error converting x-coordinate into integer")
	}

	y, error := strconv.Atoi(container[1])
	if error != nil {
		log.Fatal("There was a problem converting y-coordinate into an integer")
	}

	direction := container[2]

	return x, y, direction
}

/*
GetCurrentStatus : Returns the currnet status of the bot

@return		int					x-coordinate
@return		int					y-coordinate
@return		direction		direction object representing where the bot is facing
*/
func (bot *Bot) GetCurrentStatus() (int, int, direction.CardinalDirections) {
	return bot.xCoordinate, bot.yCoordinate, bot.faceDirection
}

/*
getCurrentXCoordinate : returns the current x-coordinate
*/
func (bot *Bot) getCurrentXCoordinate() int {
	return bot.xCoordinate
}

/*
getCurrentYCoordinate : returns the current y-coordinate
*/
func (bot *Bot) getCurrentYCoordinate() int {
	return bot.yCoordinate
}

/*
getCurrentFaceDirection : returns a string htat represents where the bot is facing

@return		direction		where the bot is facing
*/
func (bot *Bot) getCurrentFaceDirection() direction.CardinalDirections {
	return bot.faceDirection
}
