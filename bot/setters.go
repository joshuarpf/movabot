// Package bot : provides the features of the simulated bot
package bot

import (
	"movabot/direction"
)

/*
setPlacement : The process the executes the placing of a bot

@params		string		representation of where the bot is facing
*/
func (bot *Bot) setPlacement(input string) {

	if len(input) <= 0 {
		return
	}

	x, y, directionString := bot.getCoordinatesFromPlaceCommand(input)

	bot.setXCoordinate(x)
	bot.setYCoordinate(y)

	if directionString == direction.NORTH.String() {
		bot.setFaceDirection(direction.NORTH)
	} else if directionString == direction.EAST.String() {
		bot.setFaceDirection(direction.EAST)
	} else if directionString == direction.SOUTH.String() {
		bot.setFaceDirection(direction.SOUTH)
	} else if directionString == direction.WEST.String() {
		bot.setFaceDirection(direction.WEST)
	}

	bot.setPlaced(true)
}

/*
setXCoordinate : Sets the x coordinate value

@params		integer		x coordinate
*/
func (bot *Bot) setXCoordinate(input int) {
	if input < 0 {
		return
	}

	bot.xCoordinate = input
}

/*
setYCoordinate : Sets the y coordinate value

@params		integer		y coordinate
*/
func (bot *Bot) setYCoordinate(input int) {
	if input < 0 {
		return
	}

	bot.yCoordinate = input
}

/*
setFaceDirection : Sets the face direction value

@params		direction 		cardinal direction
*/
func (bot *Bot) setFaceDirection(direction direction.CardinalDirections) {
	if len(direction.String()) <= 0 {
		return
	}

	bot.faceDirection = direction
}

/*
setPlaced : set's the value of the placement of the bot

@params		boolean		input to determine if the bot has been placed or not
*/
func (bot *Bot) setPlaced(input bool) {
	if input != true && input != false {
		return
	}
	bot.placed = input
}
