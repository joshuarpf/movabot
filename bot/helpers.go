// Package bot : provides the features of the simulated bot
package bot

import (
	"log"
	"movabot/command"
	"movabot/direction"
	"movabot/helper"
)

/*
getCardinalDirections : returns an array of cardinal directions

@return		array		array of directions
*/
func getCardinalDirectionArray() []direction.CardinalDirections {

	var cardinalDirections = []direction.CardinalDirections{
		direction.NORTH,
		direction.EAST,
		direction.SOUTH,
		direction.WEST,
	}

	return cardinalDirections

}

/*
getNextDirection : method that returns the next direction of the bot

@params		currentDirection		the current facing direction
@params		directionToTurn			implies where the next direction should be

@return		direction						next direction to face
*/
func (bot *Bot) getNextDirection(currentDirection direction.CardinalDirections, directionToTurn command.Keyword) direction.CardinalDirections {
	var index int

	directionsArray := getCardinalDirectionArray()
	lastIndex := len(directionsArray) - 1

	index = helper.GetIndexFromDirectionsArray(directionsArray, currentDirection)

	// Checker
	if index == -1 {
		log.Fatal("The returned index is not a valid value. ")
	}

	if directionToTurn == command.LEFT {
		if index == 0 {
			index = lastIndex
		} else {
			index = (index - 1)
		}
	} else if directionToTurn == command.RIGHT {
		if index == lastIndex {
			index = 0
		} else {
			index = (index + 1)
		}
	}

	return directionsArray[index]
}

/*
isPlaced : Checks to see the bot has been placed

@return		boolean		identifies if the bot has been placed or not
*/
func (bot *Bot) isPlaced() bool {

	if bot.placed == true {
		return true
	}

	return false
}
