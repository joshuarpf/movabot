// Package bot : provides the features of the simulated bot
package bot

import "movabot/direction"

//Bot : provides the structure of a bot
type Bot struct {
	faceDirection            direction.CardinalDirections
	xCoordinate, yCoordinate int
	placed                   bool
}

/*
Updater : provides the interface of the bot
*/
type Updater interface {
	EvaluateInput()
	getCurrentFaceDirection() string
	GetCurrentStatus()
	getCurrentXCoordinate()
	getCurrentYCoordinate()
	getCoordinatesFromPlaceCommand()
	getNextDirection()
	isPlaced() bool
	move()
	setPlaced()
	setPlacement()
	setFaceDirection()
	setYCoordinate()
	setXCoordinate()
	showNoPlacementError()
	showReport()
	turn()
}
