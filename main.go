package main

import (
	"bufio"
	"fmt"
	"log"
	"movabot/bot"
	"movabot/command"
	"movabot/helper"
	"movabot/validator"
	"os"
	"strings"
)

/*
cleanupInput : formats the input to the acceptible upper case, without the trailing new line

@params		input		string
@output						string
*/
func cleanupInput(input string) string {

	if len(input) <= 0 {
		log.Fatal("There is no input ot be evaluated")
	}

	input = strings.Replace(input, "\n", "", -1) // the number denotes no limit in the number of occurence
	input = strings.Trim(input, " ")             // Trims the sides
	input = strings.ToUpper(input)               // converts all input to uppre case

	// Remove the trailing string
	if strings.HasPrefix(input, command.PLACE.String()) == false {
		input = helper.TrimStringsFromPrefix(input)
	}

	return input
}

/*
main : The main function that runs the program
*/
func main() {
	fmt.Println("Welcome to the bot simulator ...")

	robot := &bot.Bot{}

	run(robot)
}

/*
run : The method that runs the game

@params		object 		represents the bot object
*/
func run(robot *bot.Bot) {
	consoleReader := bufio.NewReader(os.Stdin)

	input, error := consoleReader.ReadString('\n') // reads input up to the delimiter
	if error != nil {
		log.Fatal("There was a problem reading the input string")
	}

	input = cleanupInput(input)

	isInputValid := validator.IsValidInput(input)

	if isInputValid == true {
		robot.EvaluateInput(input)
	} else {
		fmt.Printf("Invalid input: %s \n", input)
	}

	run(robot) // recursively calls the function to keep the game running
}
