// Package direction provides the structure for directions
package direction

// CardinalDirections : represents the type of the direction
type CardinalDirections int

/*
This defines the enum of the directions
*/
const (
	NORTH CardinalDirections = iota
	EAST
	SOUTH
	WEST
)

/*
GetString : returns the string representation based on the index
*/
func (index CardinalDirections) String() string {
	return [...]string{"NORTH", "EAST", "SOUTH", "WEST"}[index]
}
