package helper

import (
	"fmt"
	"log"
	"os"
	"strings"
)

/*
ExtractCoordinatesFromPlaceString : returns the coordinates from a given place string.
This assumes that the string to be given is valid
*/
func ExtractCoordinatesFromPlaceString(input string) []string {

	if len(input) <= 0 {
		log.Fatal("The accepted place string does cannot be used as an input")
	}

	input = strings.ReplaceAll(input, "PLACE", "")
	input = strings.ReplaceAll(input, " ", "")

	container := strings.Split(input, ",")

	if len(container) != 3 {
		fmt.Println("There is a problem with the derived placement format")
		os.Exit(0)
	}

	return container
}

/*
TrimStringsFromPrefix : strips out the trailing strings except for the prefix
*/
func TrimStringsFromPrefix(input string) string {
	if result := strings.Index(input, " "); result != -1 {
		return input[:result]
	}
	return input
}
