// Package helper : provides a collection of helper methods usable by the entire app
package helper

import "movabot/direction"

/*
GetIndexFromDirectionsArray : returns the index that corresponds to the string from a given array

@params 		array 			array of directions
@params			direction 	direction to find

@return 		int					index
*/
func GetIndexFromDirectionsArray(array []direction.CardinalDirections, needle direction.CardinalDirections) int {
	for index, element := range array {
		if element == needle {
			return index
		}
	}

	return -1
}
