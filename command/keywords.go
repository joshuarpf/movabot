// Package command : provides the structure of all acceptible commands
package command

// Keyword : type for the commands enum
type Keyword int

/*
This defines the enum
*/
const (
	LEFT Keyword = iota
	MOVE
	PLACE
	QUIT
	REPORT
	RIGHT
)

/*
String : returns the sring representation of the enum
*/
func (index Keyword) String() string {
	return [...]string{"LEFT", "MOVE", "PLACE", "QUIT", "REPORT", "RIGHT"}[index]
}
